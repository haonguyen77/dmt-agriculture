/**
  ******************************************************************************
  * @file    button.h 
  * @author  Hao Nguyen - demeter
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BUTTON_H
#define __BUTTON_H

/* Includes ------------------------------------------------------------------*/
#include "AppSource/port.h"

/* Private define ------------------------------------------------------------*/
#define PRESSED          1
#define NOT_PRESSED      0
#define PERIODIC_SCAN 	 10
#define PARAMETER_FILTER (uint8_t)0x0F //N consecutive states = 5

/* Private typedef -----------------------------------------------------------*/
typedef struct{
	uint8_t 		  acc;			  //The button accumulator
	uint8_t 			isPressed;	//The button states
	uint8_t				isLongPressed;
	GPIO_TypeDef* port;			
	uint32_t 			pin;
}btnTypedef;

typedef enum{
	INACTIVE,
	ACTIVE
}inputState;

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void buttonScan(btnTypedef* btn);
void clearButtonState(btnTypedef *btn);
void buttonLoop(void);

uint8_t filterButtonN5(btnTypedef* btn);
inputState buttonReadInput(btnTypedef* btn);
#endif /* __BUTTON_H */