/**
  ******************************************************************************
  * @file    control.h 
  * @author  Hao Nguyen - demeter
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONTROL_H
#define __CONTROL_H

/* Includes ------------------------------------------------------------------*/
#include "AppSource/port.h"

/* Private define ------------------------------------------------------------*/
#define ItemStatus						 FlagStatus
#define RUN 									 SET
#define STOP									 RESET
#define ON										 SET
#define OFF										 RESET

#define NUM_CASE_CONTROL_PUMP	 4
#define NUM_CASE_CONTROL_LIFAN 4
/* Private typedef -----------------------------------------------------------*/
typedef struct{
	ItemStatus status;
	GPIO_TypeDef* port;
	uint32_t pin;
}itemTypedef;

/* Extern variables ----------------------------------------------------------*/
extern itemTypedef pump;
extern itemTypedef light;
extern itemTypedef fan1;
extern itemTypedef fan2;

/* Private function prototypes -----------------------------------------------*/
void applicationInit(void);
void checkCondition(void);
void controlPump(void);
void controlLightFan(void);
void handleButton(void);
void pumpWorking(ItemStatus status);
void fan1Working(ItemStatus status);
void fan2Working(ItemStatus status);
void lightWorking(ItemStatus status);
#endif /* __CONTROL_H */