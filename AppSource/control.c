/**
  ******************************************************************************
  * @file    control.c 
  * @author  Hao Nguen - Demeter
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "AppSource/control.h"
#include "AppSource/time.h"
#include "AppSource/button.h"

/* Private variables ---------------------------------------------------------*/
extern btnTypedef swPump;
extern btnTypedef swFan1;
extern btnTypedef swFan2;
extern btnTypedef swLight;

itemTypedef pump  = {STOP, GPIOB, GPIO_PIN_12};
itemTypedef fan1  = {STOP, GPIOB, GPIO_PIN_13};
itemTypedef light = {OFF, GPIOB, GPIO_PIN_14};
itemTypedef fan2  = {STOP, GPIOB, GPIO_PIN_15};

Timeout_Type pumPeriodicWorking;

uint8_t pumpCaseTime[NUM_CASE_CONTROL_PUMP] = {7,12,15,18};
uint8_t lifanCaseBeginTime[NUM_CASE_CONTROL_LIFAN] = {6};
uint8_t lifanCaseEndTime[NUM_CASE_CONTROL_LIFAN] = {18};

FlagStatus flagControl = RESET;

/* Private function prototypes -----------------------------------------------*/
void applicationInit(void)
{
	InitTimeout(&pumPeriodicWorking, getSecondTick, 60);
}

void checkCondition(void){
	uint8_t index;
	static uint8_t old_hours = 0;
	uint8_t hours = getHours();
	
	if(old_hours != hours)
	{
		INFO("Checking Time Conditions\n");
		old_hours = hours;
		
		for(index = 0; index < NUM_CASE_CONTROL_PUMP; index ++){
			if(hours == pumpCaseTime[index]){
				flagControl = SET;
				InitTimeout(&pumPeriodicWorking, getSecondTick, 60);
				
				INFO("Pump is Running\n");
			}				
		}

		for(index = 0; index < NUM_CASE_CONTROL_LIFAN; index ++){
			if(hours == lifanCaseBeginTime[index]){
				fan1.status = RUN;
				fan2.status = RUN;
				light.status = ON;
				
				INFO("Light and Fans are Running\n");
			}else if(hours == lifanCaseEndTime[index]){
				fan1.status = STOP;
				fan2.status = STOP;
				light.status = OFF;
				INFO("Light and Fans Stop\n");
			}			
		}
	}
}

void controlPump(void){
	if(flagControl == SET){
		pump.status = RUN;
		if(CheckTimeout(&pumPeriodicWorking, getSecondTick)== TIMEOUT)
		{
			flagControl = RESET;
			pump.status = STOP;
			
			INFO("Pump Stop\n");
		}
		pumpWorking(pump.status);
	}
}

void controlLightFan(void){
	fan1Working(fan1.status);
	fan2Working(fan2.status);
	lightWorking(light.status);
}

void handleButton(void){
	if(swPump.isPressed){
		clearButtonState(&swPump);
		
		if(pump.status == RUN){
			pump.status = STOP;
		}else{
			pump.status = RUN;
		}
		
		pumpWorking(pump.status);
	}
	
	if(swFan1.isPressed){
		clearButtonState(&swFan1);
		
		if(fan1.status == RUN){
			fan1.status = STOP;
		}else{
			fan1.status = RUN;
		}
		
		fan1Working(fan1.status);
	}
	
	if(swFan2.isPressed){
		clearButtonState(&swFan2);
		
		if(fan2.status == RUN){
			fan2.status = STOP;
		}else{
			fan2.status = RUN;
		}
		
		fan2Working(fan2.status);
	}
	
	if(swLight.isPressed){
		clearButtonState(&swLight);
		
		if(light.status == ON){
			light.status = OFF;
		}else{
			light.status =ON;
		}
		
		lightWorking(light.status);
	}
}