/**
  ******************************************************************************
  * @file    time.c 
  * @author  
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "AppSource/time.h"

uint32_t secondTick;

/*------------------------------------------------------------------------------
@brief  : InitTimeout
@param  : None
@return : None
-------------------------------------------------------------------------------*/
void InitTimeout(Timeout_Type *t, GetTickTypedef GetTick, uint32_t timeout){
	t->start_time = GetTick();
	t->timeout = timeout;
	t->crc = t->start_time + t->timeout;
}

/*------------------------------------------------------------------------------
@brief  : CheckTimeout
@param  : None
@return : 0: timeout
          1: time remain
-------------------------------------------------------------------------------*/
uint32_t CheckTimeout(Timeout_Type *t, GetTickTypedef GetTick)
{
	uint32_t u32temp, u32temp1;
    
	u32temp = t->start_time + t->timeout;
//	if(u32temp != t->crc)
//  {
//		NVIC_SystemReset();   // reset system
//  }
    
	u32temp = GetTick();
	t->crc = t->start_time + t->timeout;
	if(u32temp >= t->start_time)
		u32temp1 = u32temp - t->start_time;
	else
		u32temp1 = (0xFFFFFFFF - t->start_time) + u32temp;
	if(u32temp1 >= t->timeout) return 0;    // timeout
	return (t->timeout - u32temp1);
}

void incSecondTick(void){
	static uint16_t count = 0;
	if(count < PERIODIC_SECOND_TICK){
		count++;
	}else{
		count = 0;
		secondTick++;
	}
}

uint32_t getSecondTick(void)
{
	return secondTick;
}