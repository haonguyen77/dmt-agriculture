/**
  ******************************************************************************
  * @file    port.c 
  * @author  Hao Nguen - Demeter
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "AppSource/control.h"
#include "AppSource/time.h"
#include "AppSource/button.h"

/* Private variables ---------------------------------------------------------*/
btnTypedef swPump  = {0, NOT_PRESSED, NOT_PRESSED, GPIOA, GPIO_PIN_0};
btnTypedef swFan1  = {0, NOT_PRESSED, NOT_PRESSED, GPIOA, GPIO_PIN_1};
btnTypedef swLight = {0, NOT_PRESSED, NOT_PRESSED, GPIOB, GPIO_PIN_0};
btnTypedef swFan2  = {0, NOT_PRESSED, NOT_PRESSED, GPIOB, GPIO_PIN_1};

extern RTC_HandleTypeDef hrtc;

/* Private function prototypes -----------------------------------------------*/
uint8_t getHours(void){
	RTC_TimeTypeDef currentTime;
	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	return currentTime.Hours;
}

uint8_t getMinutes(void){
	RTC_TimeTypeDef currentTime;
	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	return currentTime.Minutes;
}

uint8_t getSeconds(void){
	RTC_TimeTypeDef currentTime;
	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	return currentTime.Seconds;
}

void pumpWorking(ItemStatus status){
	if(status == RUN){
		HAL_GPIO_WritePin(pump.port, pump.pin, GPIO_PIN_SET);
	}else{
		HAL_GPIO_WritePin(pump.port, pump.pin, GPIO_PIN_RESET);
	}
}

void fan1Working(ItemStatus status){
	if(status == RUN){
		HAL_GPIO_WritePin(fan1.port, fan1.pin, GPIO_PIN_SET);
	}else{
		HAL_GPIO_WritePin(fan1.port, fan1.pin, GPIO_PIN_RESET);
	}	
}

void fan2Working(ItemStatus status){
	if(status == RUN){
		HAL_GPIO_WritePin(fan2.port, fan2.pin, GPIO_PIN_SET);
	}else{
		HAL_GPIO_WritePin(fan2.port, fan2.pin, GPIO_PIN_RESET);
	}	
}

void lightWorking(ItemStatus status){
	if(status == ON){
		HAL_GPIO_WritePin(light.port, light.pin, GPIO_PIN_SET);
	}else{
		HAL_GPIO_WritePin(light.port, light.pin, GPIO_PIN_RESET);
	}	
}

inputState buttonReadInput(btnTypedef* btn){
	if(HAL_GPIO_ReadPin(btn->port, btn->pin) == GPIO_PIN_RESET){
		return ACTIVE;
	}else{
		return INACTIVE;
	}
}

void buttonLoop(void){
	static uint16_t count = 0;
	
	if(count < PERIODIC_SCAN){
		count ++;
	}else{
		count = 0;
		buttonScan(&swPump);
		buttonScan(&swFan1);
		buttonScan(&swFan2);
		buttonScan(&swLight);
	}
}