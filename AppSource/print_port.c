/**
  ******************************************************************************
  * @file    print_port.c 
  * @author  Hao Nguen - Demeter
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "stm32f1xx_hal.h"

extern UART_HandleTypeDef huart2;

#define USART_DEBUG	huart2

#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END Private defines */
	
/*******************************************************************************
Function name: PUTCHAR_PROTOTYPE
Decription:	Retargets the C library printf function to the USART.
Input: None
Output: None
*******************************************************************************/
PUTCHAR_PROTOTYPE
{
		uint8_t data = (uint8_t)ch;
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    HAL_UART_Transmit(&USART_DEBUG, &data, 1, 1);
	
    return ch;
}	