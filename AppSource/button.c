/**
  ******************************************************************************
  * @file    button.c 
  * @author  Hao Nguen - Demeter
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "AppSource/button.h"

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
uint8_t filterButtonN5(btnTypedef* btn){
	btn->acc <<= 1;
	if(buttonReadInput(btn) == ACTIVE){
		btn->acc |= 0x01;
	}
	return btn->acc;
}

void buttonScan(btnTypedef* btn){
	if(filterButtonN5(btn) == PARAMETER_FILTER){
		btn->isPressed = PRESSED;
	}
}	

void clearButtonState(btnTypedef *btn){
	btn->isPressed = NOT_PRESSED;
	btn->isLongPressed = NOT_PRESSED;
}