/**
  ******************************************************************************
  * @file    time.h 
  * @author  Hao Nguyen - demeter
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIME_H
#define __TIME_H

#include "AppSource/port.h"

#define PERIODIC_SECOND_TICK 1000

#define TIMEOUT		0
#define TIMEIN		1

typedef struct {
	uint32_t start_time; 		
	uint32_t timeout;
	uint32_t crc; 
} Timeout_Type;

typedef uint32_t(*GetTickTypedef)(void);

/* Private function prototypes -----------------------------------------------*/
/*------------------------------------------------------------------------------
@brief  : InitTimeout
@param  : None
@return : None
-------------------------------------------------------------------------------*/
void InitTimeout(Timeout_Type *t, GetTickTypedef GetTick, uint32_t timeout);

/*------------------------------------------------------------------------------
@brief  : CheckTimeout
@param  : None
@return : 0: timeout
          1: time remain
-------------------------------------------------------------------------------*/
uint32_t CheckTimeout(Timeout_Type *t, GetTickTypedef GetTick);
uint32_t getSecondTick(void);
void incSecondTick(void);
uint8_t getHours(void);
uint8_t getMinutes(void);
uint8_t getSeconds(void);
#endif